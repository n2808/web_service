﻿using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonH.Helper
{
    public class SplitProducto_helper
    {

        /// <summary>
        /// devuelve codigo del producto a trabajar, para guardarlo en presence.
        /// </summary>
        /// <param name="Producto"></param>
        /// <returns></returns>
        public  string split(string value,string splitter ="*")
        {
            try
            {
                if (value == null)
                    return null;
                int Refecenciaproductocodigo = 0;
                var codigosReferenciaProducto = value.Split(splitter.ToCharArray()[0]);
                if (codigosReferenciaProducto != null && codigosReferenciaProducto[0] != null)
                {

                    return codigosReferenciaProducto[0];
                }

                return null;

            }
            catch (Exception ex)
            {

                return null;
            }


        }

    }
}
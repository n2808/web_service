﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Internal.Logs;



namespace Presence_helper
{

    public class PresenceParams
    {

        private static leads_claro_colombia.ServicePresence.PcowsServiceClient WS;
        public int servicio { get; set; }
        public int carga { get; set; }
        public int sourceId { get; set; } = Math.Abs(unchecked((int)DateTime.Now.Ticks));
        public string nombreCliente { get; set; }
        public string telefono { get; set; }
        public string Comentarios { get; set; }
        public string Cogido1producto { get; set; }

        public string CustomData2 { get; set; }

        public string CustomData3 { get; set; }
        /**
            * METODO PARA INSERTAR LA LLAMDA A PRESENCE
            */
        public string InsertarPresence(PresenceParams P)
        {
            int resp_ws;
            string Respuesta;
            try
            {

                /*
                * CODIGO ORIGINAL.
                */

                //WS = new ServicePresence.PcowsServiceClient();
                //resp_ws = WS.InsertOutboundRecord(P.servicio, P.carga, P.sourceId, P.nombreCliente, P.telefono.ToString(), 1, DateTime.Now, 0, 100, P.Comentarios);

                //Respuesta = "1";

                /*
                 * PRUEBA DE WS SERVICES con custom data.
                 */



                WS = new leads_claro_colombia.ServicePresence.PcowsServiceClient();

                resp_ws = WS.InsertOutboundRecord4(

                    P.servicio,//int ServiceId,
                     P.carga,         //int LoadId,
                      P.sourceId,         //int SourceId,
                      P.nombreCliente,        //string Name,
                      null,        //string TimeZone,
                        1,      //int Status,
                       P.telefono.ToString(),       //string Phone,
                       null,       //string PhoneTimeZone,
                       null,       //string AlternativePhones,
                       null,       //string AlternativePhoneDescriptions,
                       null,       //string AlternativePhoneTimeZones,
                       DateTime.Now,       //System.DateTime ScheduleDate,
                        0,      //long CapturingAgent,
                        100,      //int Priority,
                        P.Comentarios,      //string Comments,
                        P.Cogido1producto,                   //string CustomData1,
                        P.CustomData2,                   //string CustomData2,
                        P.CustomData3, //string CustomData3,
                        null,                   //string CallerId,
                        null,                   //string CallerName,
                         false                  //bool AutomaticTimeZoneDetection
                    );

                Respuesta = "1";



            }
            catch (Exception ex)
            {
                Respuesta = "Error al Registrar Llamada en Presence." + ex.Message;
            }
            return Respuesta;
        }


    }
}
﻿using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonH.Helper
{
    public class VerificarTelefono_helper
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        public string Verificar(string Telefono, string Ciudad)
        {

            // VERIFICAR LA INSTANCIA
            int largo = Telefono.Length;

            if (largo > 10)
                Telefono = CutToTenDigits(Telefono);

            if (largo == 10)
                Telefono = TenDigits(Telefono);


            if (largo == 7 && Ciudad != null)
            {

                Indicativos Ind = db.Indicativos.Where(c => c.ciudad.Trim() == Ciudad.Trim()).FirstOrDefault();

                if (Ind != null)
                    Telefono = AddPrefix(Telefono, Ind?.Indicativo.ToString());

            }
            else if (largo == 8)
            {

                Telefono = EightDigits(Telefono);
            }


            return Telefono;
        }


        public string TenDigits(string Telefono)
        {
            return Telefono.Substring(0, 1) == "3" ?
                String.Concat("3", Telefono) :
                String.Concat("2", Telefono);
        }


        public string CutToTenDigits(string Telefono)
        {
            return TenDigits(
                Telefono.Substring(Telefono.Length - 10, 10)
                );
        }

        public string AddPrefix(string Telefono, string indicativo)
        {
            return String.Concat("260", indicativo, Telefono);
        }
        public string EightDigits(string Telefono)
        {
            return String.Concat("260", Telefono);
        }

    }
}
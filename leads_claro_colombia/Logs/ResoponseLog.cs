﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Internal.Logs
{
    public class ResponseLog
    {
        public int Estado { get; set; }
        public string Mensaje { get; set; }
    }

}
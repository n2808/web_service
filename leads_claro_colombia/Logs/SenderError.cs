﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Core.Internal.Logs
{
 
        public class log 
        {
            public string Nombre { get; set; }
            public string Descripcion1 { get; set; }
            public string Descripcion2 { get; set; }
            public string Descripcion3 { get; set; }
            public string Descripcion4 { get; set; }
            public string log_json { get; set; }
            public int? CategoriaId { get; set; }
            public int? TipId { get; set; }
            public Guid? TipoGuid { get; set; }


        public int seccionId { get; set; }
            /*
             * 1 - info
             * 2 - Success
             * 3 - warning
             * 4 - error
             */

        public TipoError TipoError { get; set; }


        public ResponseLog Enviar(log Log)
        {
            ResponseLog respuesta = new ResponseLog() { };
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.5.152:44458/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<log>("InsertarLog", Log);
                postTask.Wait();
                var result = postTask.Result;



                if (result.IsSuccessStatusCode)
                {
                    respuesta.Estado = 1;
                    respuesta.Mensaje = result.Content.ToString();
                }
                else
                {
                    respuesta.Estado = 2;
                    respuesta.Mensaje = result.Content.ToString();
                }
            }

            return respuesta;
            

        }

    }

    public enum TipoError
    {
        info = 1,
        success = 2,
        warning = 3,
        error = 4

    }





}
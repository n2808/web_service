﻿using System.Web;
using System.Web.Mvc;

namespace leads_claro_colombia
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

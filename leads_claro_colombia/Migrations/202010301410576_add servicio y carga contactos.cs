namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addservicioycargacontactos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contactos", "Servicio", c => c.String());
            AddColumn("dbo.Contactos", "Carga", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contactos", "Carga");
            DropColumn("dbo.Contactos", "Servicio");
        }
    }
}

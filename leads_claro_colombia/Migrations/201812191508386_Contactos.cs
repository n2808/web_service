namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contactos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contactos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        nombres = c.String(nullable: false),
                        telefono = c.String(nullable: false),
                        Descripcion1 = c.String(),
                        Descripcion2 = c.String(),
                        PresenceMessage = c.String(),
                        SourceIdPresence = c.String(),
                        CampaignId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campaigns", t => t.CampaignId, cascadeDelete: true)
                .Index(t => t.CampaignId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contactos", "CampaignId", "dbo.Campaigns");
            DropIndex("dbo.Contactos", new[] { "CampaignId" });
            DropTable("dbo.Contactos");
        }
    }
}

namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiocargaaint : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Campaigns", "Carga", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Campaigns", "Carga", c => c.String());
        }
    }
}

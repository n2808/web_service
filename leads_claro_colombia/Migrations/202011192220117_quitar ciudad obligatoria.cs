namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class quitarciudadobligatoria : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "ciudad", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "ciudad", c => c.String(nullable: false));
        }
    }
}

namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contactosaddplanoperadorycliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contactos", "cliente", c => c.String());
            AddColumn("dbo.Contactos", "Plan", c => c.String());
            AddColumn("dbo.Contactos", "Operador", c => c.String());
            DropColumn("dbo.Contactos", "Descripcion1");
            DropColumn("dbo.Contactos", "Descripcion2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contactos", "Descripcion2", c => c.String());
            AddColumn("dbo.Contactos", "Descripcion1", c => c.String());
            DropColumn("dbo.Contactos", "Operador");
            DropColumn("dbo.Contactos", "Plan");
            DropColumn("dbo.Contactos", "cliente");
        }
    }
}

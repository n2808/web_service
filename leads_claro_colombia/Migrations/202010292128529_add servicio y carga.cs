namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addservicioycarga : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Servicio", c => c.String());
            AddColumn("dbo.Leads", "Carga", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leads", "Carga");
            DropColumn("dbo.Leads", "Servicio");
        }
    }
}

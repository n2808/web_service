namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddtablaCampaign : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campaigns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        Tiempo_ACW = c.Int(nullable: false),
                        Second_Tab = c.String(),
                        Tab_Names = c.String(),
                        Servicio = c.Int(nullable: false),
                        Carga = c.String(),
                        Prefijo = c.String(),
                        NumCamposOpcionales = c.Int(),
                        PrefijoCelular = c.Int(),
                        DigitosCelular = c.Int(),
                        PrefijoFijo = c.Int(),
                        DigitosFijo = c.Int(),
                        SourceId = c.Int(),
                        token = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Leads", "CampaignId", c => c.Int());
            CreateIndex("dbo.Leads", "CampaignId");
            AddForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns");
            DropIndex("dbo.Leads", new[] { "CampaignId" });
            DropColumn("dbo.Leads", "CampaignId");
            DropTable("dbo.Campaigns");
        }
    }
}

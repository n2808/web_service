namespace leads_claro_colombia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_indicativos_ciudad : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Indicativos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Indicativo = c.Int(nullable: false),
                        ciudad = c.String(),
                        departamento = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Indicativos");
        }
    }
}

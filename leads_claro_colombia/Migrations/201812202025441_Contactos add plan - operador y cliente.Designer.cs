// <auto-generated />
namespace leads_claro_colombia.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Contactosaddplanoperadorycliente : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Contactosaddplanoperadorycliente));
        
        string IMigrationMetadata.Id
        {
            get { return "201812202025441_Contactos add plan - operador y cliente"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

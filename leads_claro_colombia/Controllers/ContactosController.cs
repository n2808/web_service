﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using Presence_helper;

namespace leads_claro_colombia.Controllers
{
    public class ContactosController : ApiController
    {

        // code for presence
        private static ServicePresence.PcowsServiceClient WS;
        public PresenceParams PresenceCarga = new PresenceParams() { };
        public int CampaignId = 4;
        Campaign campaign;


        public ContactosController()
        {
            Campaign campaign = db.Campaign.Find(CampaignId);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Contactos
        public IQueryable<Contactos> GetContactos()
        {
            return db.Contactos;
        }

        // GET: api/Contactos/5
        [ResponseType(typeof(Contactos))]
        public IHttpActionResult GetContactos(int id)
        {
            Contactos contactos = db.Contactos.Find(id);
            if (contactos == null)
            {
                return NotFound();
            }

            return Ok(contactos);
        }

        // PUT: api/Contactos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutContactos(int id, Contactos contactos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contactos.Id)
            {
                return BadRequest();
            }

            db.Entry(contactos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Contactos
        //[ResponseType(typeof(Contactos))]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //[Route("api/IngresarContacto")]
        //[HttpPost]
        public IHttpActionResult PostContactos(Contactos contactos)
        {
            if (!ModelState.IsValid || contactos == null)
            {
                return BadRequest(ModelState);
            }
            if (contactos.telefono.Length == 10)
            {
                contactos.telefono = "3" + contactos.telefono;
            }

            PresenceCarga.nombreCliente = contactos.nombres;
            PresenceCarga.telefono = contactos.telefono;

            if(contactos.CampaignId > 0)
            {
                campaign = db.Campaign.Find(contactos.Id);
                PresenceCarga.carga = campaign.Carga;
                PresenceCarga.servicio = campaign.Servicio;
            }






            string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
            contactos.PresenceMessage = RespuestaPresence;
            contactos.SourceIdPresence = PresenceCarga.sourceId.ToString();
            contactos.CampaignId = CampaignId;
            db.Contactos.Add(contactos);
            db.SaveChanges();

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);

        }

        // DELETE: api/Contactos/5
        [ResponseType(typeof(Contactos))]
        public IHttpActionResult DeleteContactos(int id)
        {
            Contactos contactos = db.Contactos.Find(id);
            if (contactos == null)
            {
                return NotFound();
            }

            db.Contactos.Remove(contactos);
            db.SaveChanges();

            return Ok(contactos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactosExists(int id)
        {
            return db.Contactos.Count(e => e.Id == id) > 0;
        }
    }
   }
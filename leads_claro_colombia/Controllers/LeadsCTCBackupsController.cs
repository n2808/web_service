﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using Core.Internal.Logs;
using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using System.IO;
using System.Text;
using System.Web;
using CommonH.Helper;

namespace leads_claro_colombia.Controllers
{

    /*
        *
     */
    public class CTCBackupsController : ApiController
    {

        private static ServicePresence.PcowsServiceClient WS;
        public PresenceParams PresenceCarga = new PresenceParams() { };
        private VerificarTelefono_helper VerificarTelefono = new VerificarTelefono_helper() { };
        private SplitProducto_helper sppliter = new SplitProducto_helper() { };
        public int CampaignId = 21;
        Campaign campaign;

        /*
            * WEB SERVICE PARA CTC Y WEBCB NUEVAS URLS PARA PRODUCCION Y PARA BACKUP
         */
        public CTCBackupsController()
        {
            campaign = db.Campaign.Find(CampaignId);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
        }
        
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Leads
        public IQueryable<Leads> GetLeads()
        {
            return db.Leads;
        }

        // GET: api/Leads/5
        [ResponseType(typeof(Leads))]
        public IHttpActionResult GetLeads(int id)
        {
            Leads leads = db.Leads.Find(id);
            if (leads == null)
            {
                return NotFound();
            }

            return Ok(leads);
        }

        // PUT: api/Leads/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLeads(int id, Leads leads)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != leads.Id)
            {
                return BadRequest();
            }

            db.Entry(leads).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeadsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }




        /*------------------------------------------
         * API PARA INGRESAR EL SERVICIO DE TECNOLOGIA 
         -------------------------------------------*/

        // POST: api/Leads
        //[ResponseType(typeof(Leads))]
        [Route("Backup/ingresarTecnologia")]
        public IHttpActionResult PostTecnologia(Leads leads)
        {
            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(19);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;

            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "Nuevo lead de ingresar Tecnologia",
                log_json = serializar.Serialize(leads),
                seccionId = 2,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };

            /*------------------------------------------
             * VERIFICANDO LARGO TELEFONO.
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);

            // insertar registro en presence

            PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
            PresenceCarga.telefono = leads.telefono;
            PresenceCarga.CustomData2 = leads.producto;
            if (leads.producto != null && leads.producto.Length > 100)
              PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);
            else
              PresenceCarga.Comentarios = "pro_ws=" + leads.producto;


            if (!ModelState.IsValid)
            {
                      var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };


                    /*| * INSERTANDO EN LA TABLA DE LOGS |*/
                    Log.log_json = serializar.Serialize(leads);
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);
                    return Ok(d);
                }


            try
            {


                /*------------------------------------------
                 * VERIFICAR EN PRESENCE.
                 -------------------------------------------*/
                PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);

                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;
                

                /*| * INSERTANDO EN LA TABLA DE LOGS INFO|*/
                Log.TipoError = TipoError.info;
                Log.log_json = serializar.Serialize(leads);
                Log.Descripcion2 = leads.nombres;
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);

            
                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsTecnologia"+ fecha +".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    //INSERTAR EN BD CLAROCOL.
                    //queryString data = db.Database.SqlQuery<queryString>("P_crearclienteLeads @CLI_MSISDN = {0},@CLI_DOCUMENTO ={1},@CLI_NOMBRE ={2},@CLI_LEADS_CIUDAD ={3},@CLI_LEADS_PRODUCTO ={4},@CLI_LEADS_UTMCAMPAIGN ={5},@CLI_LEADS_REFERENCIAPRODUCTO={6}", leads.telefono, leads.cedula, leads.nombres, leads.ciudad, leads.producto, leads.utm_campaign, leads.referencia_producto
                    //    ).FirstOrDefault();
                }
                catch (Exception ext)
                {
                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }

            }


            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }



        // POST: api/Leads
        //[ResponseType(typeof(Leads))]
        [Route("Backup/ingresaSolicitud")]
        public IHttpActionResult  PostLeads(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "Nuevo lead de ingresar solicitud",
                log_json = serializar.Serialize(leads),
                seccionId = 1,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };

            /*------------------------------------------
             * VERIFICANDO LARGO TELEFONO.
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);



            try
            {


                if (!ModelState.IsValid)
                {

                    var  errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select  new { Message = item.Value.Errors[0].ErrorMessage , field = item.Key } ).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if(leads.producto != null  && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" +  leads.producto.Substring(0 , 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }

                /*------------------------------------------
                 * VERIFICAR EN PRESENCE.
                 -------------------------------------------*/
                PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);


                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;

                Log.TipoError = TipoError.info;
                Log.Descripcion1 = leads.telefono;
                Log.Descripcion2 = leads.nombres;
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);

            
                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

            }
            //queryString data = db.Database.SqlQuery<queryString>("P_crearclienteLeads @CLI_MSISDN = {0},@CLI_DOCUMENTO ={1},@CLI_NOMBRE ={2},@CLI_LEADS_CIUDAD ={3},@CLI_LEADS_PRODUCTO ={4},@CLI_LEADS_UTMCAMPAIGN ={5},@CLI_LEADS_REFERENCIAPRODUCTO={6}", leads.telefono, leads.cedula, leads.nombres, leads.ciudad, leads.producto, leads.utm_campaign, leads.referencia_producto
            //).FirstOrDefault();
            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v) ;
        }

        public class queryString
        {

            public string Data { get; set; }
        }

        // DELETE: api/Leads/5
        [ResponseType(typeof(Leads))]
        public IHttpActionResult DeleteLeads(int id)
        {
            Leads leads = db.Leads.Find(id);
            if (leads == null)
            {
                return NotFound();
            }

            db.Leads.Remove(leads);
            db.SaveChanges();

            return Ok(leads);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeadsExists(int id)
        {
            return db.Leads.Count(e => e.Id == id) > 0;
        }



        public class PresenceParams
        {
            public int servicio { get; set; }
            public int carga { get; set; }
            public int sourceId { get; set; } = Math.Abs(unchecked((int)DateTime.Now.Ticks));
            public string nombreCliente { get; set; }
            public string telefono { get; set; }
            public string Comentarios { get; set; }
            public string Cogido1producto { get; set; }

            public string CustomData2 { get; set; }
             /**
                 * METODO PARA INSERTAR LA LLAMDA A PRESENCE
                 */
            public string InsertarPresence(PresenceParams P)
            {
                int resp_ws;
                string Respuesta;
                try
                {

                    /*
                    * CODIGO ORIGINAL.
                    */

                    //WS = new ServicePresence.PcowsServiceClient();
                    //resp_ws = WS.InsertOutboundRecord(P.servicio, P.carga, P.sourceId, P.nombreCliente, P.telefono.ToString(), 1, DateTime.Now, 0, 100, P.Comentarios);

                    //Respuesta = "1";

                    /*
                     * PRUEBA DE WS SERVICES con custom data.
                     */



                    WS = new ServicePresence.PcowsServiceClient();

                    resp_ws = WS.InsertOutboundRecord4(

                        P.servicio,//int ServiceId,
                         P.carga,         //int LoadId,
                          P.sourceId,         //int SourceId,
                          P.nombreCliente,        //string Name,
                          null,        //string TimeZone,
                            1,      //int Status,
                           P.telefono.ToString(),       //string Phone,
                           null,       //string PhoneTimeZone,
                           null,       //string AlternativePhones,
                           null,       //string AlternativePhoneDescriptions,
                           null,       //string AlternativePhoneTimeZones,
                           DateTime.Now,       //System.DateTime ScheduleDate,
                            0,      //long CapturingAgent,
                            100,      //int Priority,
                            P.Comentarios,      //string Comments,
                            P.Cogido1producto,                   //string CustomData1,
                            P.CustomData2,                   //string CustomData2,
                            null, //string CustomData3,
                            null,                   //string CallerId,
                            null,                   //string CallerName,
                             false                  //bool AutomaticTimeZoneDetection
                        );

                    Respuesta = "1";



                }
                catch (Exception ex)
                {
                    Respuesta = "Error al Registrar Llamada en Presence." + ex.Message;
                }
                return Respuesta;
            }


        }
    }

 


}
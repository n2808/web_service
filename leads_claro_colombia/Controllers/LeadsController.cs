﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using Core.Internal.Logs;
using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using leads_claro_colombia.ViewModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Http.Cors;
using CommonH.Helper;
using Presence_helper;

namespace leads_claro_colombia.Controllers
{
    public class LeadsController : ApiController
    {

        public PresenceParams PresenceCarga = new PresenceParams() { };
        // verificar telefono
        private VerificarTelefono_helper VerificarTelefono = new VerificarTelefono_helper() { };
        private SplitProducto_helper sppliter = new SplitProducto_helper() { };
        public int CampaignId = 2;
        Campaign campaign;

        public LeadsController()
        {
            campaign = db.Campaign.Find(CampaignId);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
        }
        

        //// conexion a presence.
        //public SqlConnection conexion()
        //{
        //    SqlConnection conn;

        //    try
        //    {
        //        conn = new SqlConnection("Data Source=172.16.5.153;Initial Catalog=DBWebServicesUno27;Persist Security Info=True;User ID=UsrWebServiceUno27;Password=UsrWebServiceUno27123");
        //        //conn.Open();
        //        return conn;
        //    }
        //    catch (Exception)
        //    {

        //        return null;
        //    }
        //}


        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Leads
        public IQueryable<Leads> GetLeads()
        {
            return db.Leads;
        }

        // GET: api/Leads/5
        [ResponseType(typeof(Leads))]
        public IHttpActionResult GetLeads(int id)
        {
            Leads leads = db.Leads.Find(id);
            if (leads == null)
            {
                return NotFound();
            }

            return Ok(leads);
        }

        // PUT: api/Leads/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLeads(int id, Leads leads)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != leads.Id)
            {
                return BadRequest();
            }

            db.Entry(leads).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeadsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }




        /*------------------------------------------
         * API PARA INGRESAR EL SERVICIO DE TECNOLOGIA 
         -------------------------------------------*/

        // POST: api/Leads
        //[ResponseType(typeof(Leads))]
        [Route("ingresarTecnologia")]
        public IHttpActionResult PostTecnologia(Leads leads)
        {


            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(10);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;

            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "Nuevo lead de ingresar Tecnologia",
                log_json = serializar.Serialize(leads),
                seccionId = 2,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };

            /*------------------------------------------
             * VERIFICANDO LARGO TELEFONO.
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono ,leads.ciudad);
            // obtener el producto.
            PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);




            // insertar registro en presence
            /*------------------------------------------
                * VERIFICAR EN PRESENCE.
            -------------------------------------------*/
            PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
            PresenceCarga.telefono = leads.telefono;
            PresenceCarga.CustomData2 = leads.producto;
            if (leads.producto != null && leads.producto.Length > 100)
              PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);
            else
              PresenceCarga.Comentarios = "pro_ws=" + leads.producto;


            if (!ModelState.IsValid)
            {
                      var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };


                    /*| * INSERTANDO EN LA TABLA DE LOGS |*/
                    Log.log_json = serializar.Serialize(leads);
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);


                    return Ok(d);
                }



            try
            {

                //PresenceCarga.Cogido1producto = Resultado de ejecucion del procedimiento

                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;
                leads.Servicio = campaign.Servicio.ToString();
                leads.Carga = campaign.Carga.ToString();
                

                /*| * INSERTANDO EN LA TABLA DE LOGS INFO|*/
                Log.TipoError = TipoError.info;
                Log.log_json = serializar.Serialize(leads);
                Log.Descripcion2 = leads.nombres;
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);

            
                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsTecnologia"+ fecha +".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    leads.Servicio = campaign.Servicio.ToString();
                    leads.Carga = campaign.Carga.ToString();
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    //INSERTAR EN BD CLAROCOL.
                    //queryString data = db.Database.SqlQuery<queryString>("P_crearclienteLeads @CLI_MSISDN = {0},@CLI_DOCUMENTO ={1},@CLI_NOMBRE ={2},@CLI_LEADS_CIUDAD ={3},@CLI_LEADS_PRODUCTO ={4},@CLI_LEADS_UTMCAMPAIGN ={5},@CLI_LEADS_REFERENCIAPRODUCTO={6}", leads.telefono, leads.cedula, leads.nombres, leads.ciudad, leads.producto, leads.utm_campaign, leads.referencia_producto
                    //    ).FirstOrDefault();

   

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = ext.Message;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = leads.nombres;
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);
                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }

            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }

        /*------------------------------------------
         * API PARA INGRESAR EL SERVICIO DE TECNOLOGIA 
         -------------------------------------------*/

        // POST: api/Leads
        //[ResponseType(typeof(Leads))]
        [Route("|")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult PostLeadsCRMClaroHogarTMKGoogleADS(WebHubGoogleVM leadsIn)
        {


            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(24);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;

            var serializar = new JavaScriptSerializer();
            
            var Log = new log()
            {
                Nombre = "Nuevo lead de google ads",
                log_json = serializar.Serialize(leadsIn),
                seccionId = 10,
                TipoError = TipoError.info
            };

            Log.Enviar(Log);

            if (leadsIn.google_key != "VW5vMjcyMDIwKisr")// -- Uno272020*++
            {

                //var m = new { codigo = 0, Mensaje = "Error" };
                //return Ok(m);
                return Unauthorized();

            }

            //string result = await Request.Content.ReadAsStringAsync();

            ResponseLog Resultado = new ResponseLog() { };

            Leads leads = new Leads();
            leads.ciudad = "sin ciudad";
            foreach (var leadIn in leadsIn.user_column_data)
            {

                if (leadIn.column_name == "User Phone")
                {
                    if (leadIn.string_value.Length > 10)
                    {
                        //leads.telefono = "3" + leadIn.string_value;
                        leads.telefono = "3" + leadIn.string_value.Substring(leadIn.string_value.Length - 10, 10);
                    }
                    if (leadIn.string_value.Length == 10)
                    {
                        leads.telefono = "3" + leadIn.string_value;
                    }
                    if (leadIn.string_value.Length == 8)
                    {
                        leads.telefono = "2" + leadIn.string_value;
                    }
                    if (leadIn.string_value.Length == 7)
                    {
                        leads.telefono = "21" + leadIn.string_value;
                    }
                }

                if (leadIn.column_name == "User Email")
                    leads.email= leadIn.string_value;
                if (leadIn.column_name == "Full Name")
                    leads.nombres= leadIn.string_value;
                if (leadIn.column_name == "Postal Code")
                    leads.ciudad= leadIn.string_value;

            }
            

            //INSERTAR EN BD BDTMKHogaresCotizador.
            SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
            "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
            SqlCommand CMD = new SqlCommand
            ("P_crearclienteLeads_HOGARES", Conn);

            CMD.CommandType = CommandType.StoredProcedure;
            CMD.Parameters.AddWithValue("@telefono", leads.telefono);
            CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
            CMD.Parameters.AddWithValue("@Email", leads.telefono);
            CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

            Conn.Open();

            var idCliente = CMD.ExecuteScalar();
            
            PresenceCarga.CustomData3= idCliente.ToString();

            // insertar registro en presence
            /*------------------------------------------
                * VERIFICAR EN PRESENCE.
            -------------------------------------------*/

            PresenceCarga.nombreCliente = leads.nombres;
            PresenceCarga.telefono = leads.telefono;
            //PresenceCarga.CustomData2 = leads.producto;
            if (leads.producto != null && leads.producto.Length > 100)
                PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);
            else
                PresenceCarga.Comentarios = "pro_ws=" + leads.producto;


            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState
                                    where item.Value.Errors.Any()
                                    select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                var d = new {};//codigo = -1, Mensaje = errorList


                /*| * INSERTANDO EN LA TABLA DE LOGS |*/
                Log.log_json = serializar.Serialize(leads);
                Log.TipoError = TipoError.warning;
                Log.Descripcion2 = serializar.Serialize(errorList);
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);


                return Ok("ok");
            }


            try
            {

                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;
                leads.Servicio = campaign.Servicio.ToString();
                leads.Carga = campaign.Carga.ToString();

                /*| * INSERTANDO EN LA TABLA DE LOGS INFO|*/
                Log.TipoError = TipoError.info;
                Log.log_json = serializar.Serialize(leads);
                Log.Descripcion2 = leads.nombres;
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);


                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();

                
            }
            catch (Exception ex)
            {
                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsGoogleAds" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    leads.Servicio = campaign.Servicio.ToString();
                    leads.Carga = campaign.Carga.ToString();
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                 



                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = ext.Message;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = leads.nombres;
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);
                    var d = new {};
                    return Ok("ok");
                }

            }
  
            //return Json("no");
            var v = new {};
            return Ok("ok");
        }


        /// <summary>
        /// Todos los Leads de hogares que ingresan al sistema.
        /// </summary>
        /// <param name="leads"></param>
        /// <returns></returns>
        [Route("ingresaSolicitudHogares")]
        public IHttpActionResult PostLeadsHogares(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "LEAD DE HOGARES",
                seccionId = 11,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            /*------------------------------------------
             * VERIFICAR EN PRESENCE.
             -------------------------------------------*/
            PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);


            /*------------------------------------------
             * ACTUALIZAR NUMERO CON EL PREFIJO
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);




            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(25);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();




            /*------------------------------------------
             * INSERTAR EN BD BDTMKHogaresCotizador.
             -------------------------------------------*/
            try
            {

                SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
                "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
                SqlCommand CMD = new SqlCommand
                ("P_crearclienteLeads_HOGARES", Conn);

                CMD.CommandType = CommandType.StoredProcedure;
                CMD.Parameters.AddWithValue("@telefono", leads.telefono);
                CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
                CMD.Parameters.AddWithValue("@Email", leads.telefono);
                CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

                Conn.Open();
                var idCliente = CMD.ExecuteScalar();
                PresenceCarga.CustomData3 = idCliente.ToString();

            }
            catch (Exception JX)
            {
                //Enviar log
                Log.TipoError = TipoError.error;
                Log.Nombre = leads.telefono;
                Log.Descripcion1 = JX.Message;
                Log.Descripcion2 = "error consultar el idcliente";
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);
            }



            Log.TipoError = TipoError.info;
            Log.Nombre = leads.telefono;
            Log.log_json = serializar.Serialize(leads);
            Log.Descripcion2 = PresenceCarga.CustomData3;
            Log.Descripcion3 = PresenceCarga.Cogido1producto;
            Log.Descripcion4 = PresenceCarga.sourceId.ToString();
            Resultado = Log.Enviar(Log);




            try
            {


                if (!ModelState.IsValid)
                {

                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if (leads.producto != null && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }




                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;



                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = leads.telefono;
                    Log.Descripcion1 = ext.Message;
                    Log.Descripcion2 = "error al insertar en presence";
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }

        /// <summary>
        /// Va dirigido a otra campaña.
        /// </summary>
        /// <param name="leads"></param>
        /// <returns></returns>
        [Route("ingresaSolicitudHogaresOrganico")]
        public IHttpActionResult PostLeadsHogaresOrganico(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "LEAD DE HOGARES",
                seccionId = 11,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            /*------------------------------------------
             * VERIFICAR EN PRESENCE.
             -------------------------------------------*/
            PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);


            /*------------------------------------------
             * ACTUALIZAR NUMERO CON EL PREFIJO
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);




            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(26);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();




            /*------------------------------------------
             * INSERTAR EN BD BDTMKHogaresCotizador.
             -------------------------------------------*/
            try
            {

                SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
                "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
                SqlCommand CMD = new SqlCommand
                ("P_crearclienteLeads_HOGARES", Conn);

                CMD.CommandType = CommandType.StoredProcedure;
                CMD.Parameters.AddWithValue("@telefono", leads.telefono);
                CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
                CMD.Parameters.AddWithValue("@Email", leads.telefono);
                CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

                Conn.Open();
                var idCliente = CMD.ExecuteScalar();
                PresenceCarga.CustomData3 = idCliente.ToString();

            }
            catch (Exception JX)
            {
                //Enviar log
                Log.TipoError = TipoError.error;
                Log.Nombre = leads.telefono;
                Log.Descripcion1 = JX.Message;
                Log.Descripcion2 = "error consultar el idcliente";
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);
            }



            Log.TipoError = TipoError.info;
            Log.Nombre = leads.telefono;
            Log.log_json = serializar.Serialize(leads);
            Log.Descripcion2 = PresenceCarga.CustomData3;
            Log.Descripcion3 = PresenceCarga.Cogido1producto;
            Log.Descripcion4 = PresenceCarga.sourceId.ToString();
            Resultado = Log.Enviar(Log);




            try
            {


                if (!ModelState.IsValid)
                {

                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if (leads.producto != null && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }




                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;



                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = leads.telefono;
                    Log.Descripcion1 = ext.Message;
                    Log.Descripcion2 = "error al insertar en presence";
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }

        /// <summary>
        /// Esta es la ruta de pruebas.
        /// </summary>
        /// <param name="leads"></param>
        /// <returns></returns>
        [Route("ingresaSolicitudHogaresOrganicoDev")]
        public IHttpActionResult PostLeadsHogaresOrganicoDev(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "LEAD DE HOGARES",
                seccionId = 11,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            /*------------------------------------------
             * VERIFICAR EN PRESENCE.
             -------------------------------------------*/
            char asterisk = '*';
            int Refecenciaproductocodigo = 0;
            var codigosReferenciaProducto = leads.referencia_producto.Split(asterisk);

            if (codigosReferenciaProducto != null && codigosReferenciaProducto[0] != null)
            {

                Int32.TryParse(codigosReferenciaProducto[0].ToString(), out Refecenciaproductocodigo);
            }

            PresenceCarga.Cogido1producto = Refecenciaproductocodigo.ToString();


            /*------------------------------------------
             * ACTUALIZAR NUMERO CON EL PREFIJO
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);




            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(28);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();




            /*------------------------------------------
             * INSERTAR EN BD BDTMKHogaresCotizador.
             -------------------------------------------*/
            try
            {

                SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
                "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
                SqlCommand CMD = new SqlCommand
                ("P_crearclienteLeads_HOGARES", Conn);

                CMD.CommandType = CommandType.StoredProcedure;
                CMD.Parameters.AddWithValue("@telefono", leads.telefono);
                CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
                CMD.Parameters.AddWithValue("@Email", leads.telefono);
                CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

                Conn.Open();
                var idCliente = CMD.ExecuteScalar();
                PresenceCarga.CustomData3 = idCliente.ToString();

            }
            catch (Exception JX)
            {
                //Enviar log
                Log.TipoError = TipoError.error;
                Log.Nombre = leads.telefono;
                Log.Descripcion1 = JX.Message;
                Log.Descripcion2 = "error consultar el idcliente";
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);
            }



            Log.TipoError = TipoError.info;
            Log.Nombre = leads.telefono;
            Log.log_json = serializar.Serialize(leads);
            Log.Descripcion2 = PresenceCarga.CustomData3;
            Log.Descripcion3 = PresenceCarga.Cogido1producto;
            Log.Descripcion4 = PresenceCarga.sourceId.ToString();
            Resultado = Log.Enviar(Log);




            try
            {


                if (!ModelState.IsValid)
                {

                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if (leads.producto != null && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }




                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;



                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = leads.telefono;
                    Log.Descripcion1 = ext.Message;
                    Log.Descripcion2 = "error al insertar en presence";
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }



       
        [Route("ingresaSolicitudHogaresPago")]
        public IHttpActionResult PostLeadsHogaresPago(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "LEAD DE HOGARES",
                seccionId = 11,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            /*------------------------------------------
             * VERIFICAR EN PRESENCE.
             -------------------------------------------*/
            PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);


            /*------------------------------------------
             * ACTUALIZAR NUMERO CON EL PREFIJO
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);




            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(27);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();




            /*------------------------------------------
             * INSERTAR EN BD BDTMKHogaresCotizador.
             -------------------------------------------*/
            try
            {

                SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
                "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
                SqlCommand CMD = new SqlCommand
                ("P_crearclienteLeads_HOGARES", Conn);

                CMD.CommandType = CommandType.StoredProcedure;
                CMD.Parameters.AddWithValue("@telefono", leads.telefono);
                CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
                CMD.Parameters.AddWithValue("@Email", leads.telefono);
                CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

                Conn.Open();
                var idCliente = CMD.ExecuteScalar();
                PresenceCarga.CustomData3 = idCliente.ToString();

            }
            catch (Exception JX)
            {
                //Enviar log
                Log.TipoError = TipoError.error;
                Log.Nombre = leads.telefono;
                Log.Descripcion1 = JX.Message;
                Log.Descripcion2 = "error consultar el idcliente";
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);
            }



            Log.TipoError = TipoError.info;
            Log.Nombre = leads.telefono;
            Log.log_json = serializar.Serialize(leads);
            Log.Descripcion2 = PresenceCarga.CustomData3;
            Log.Descripcion3 = PresenceCarga.Cogido1producto;
            Log.Descripcion4 = PresenceCarga.sourceId.ToString();
            Resultado = Log.Enviar(Log);




            try
            {


                if (!ModelState.IsValid)
                {

                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if (leads.producto != null && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }




                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;



                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = leads.telefono;
                    Log.Descripcion1 = ext.Message;
                    Log.Descripcion2 = "error al insertar en presence";
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }



       
        [Route("ingresaSolicitudHogaresPagoDev")]
        public IHttpActionResult PostLeadsHogaresPagoDev(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "LEAD DE HOGARES",
                seccionId = 11,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            /*------------------------------------------
             * VERIFICAR EN PRESENCE.
             -------------------------------------------*/
            PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);

            /*------------------------------------------
             * ACTUALIZAR NUMERO CON EL PREFIJO
             -------------------------------------------*/
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);




            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            campaign = db.Campaign.Find(29);
            PresenceCarga.carga = campaign.Carga;
            PresenceCarga.servicio = campaign.Servicio;
            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();




            /*------------------------------------------
             * INSERTAR EN BD BDTMKHogaresCotizador.
             -------------------------------------------*/
            try
            {

                SqlConnection Conn = new SqlConnection("Data Source=;integrated " +
                "Security=false;Password=usrBDTMKHogares123;User ID=UsrBDTMKHogares;Initial Catalog=BDTMKHogaresCotizador;Data Source=172.16.5.153");
                SqlCommand CMD = new SqlCommand
                ("P_crearclienteLeads_HOGARES", Conn);

                CMD.CommandType = CommandType.StoredProcedure;
                CMD.Parameters.AddWithValue("@telefono", leads.telefono);
                CMD.Parameters.AddWithValue("@Nombre", leads.nombres);
                CMD.Parameters.AddWithValue("@Email", leads.telefono);
                CMD.Parameters.AddWithValue("@zipcode", leads.ciudad);

                Conn.Open();
                var idCliente = CMD.ExecuteScalar();
                PresenceCarga.CustomData3 = idCliente.ToString();

            }
            catch (Exception JX)
            {
                //Enviar log
                Log.TipoError = TipoError.error;
                Log.Nombre = leads.telefono;
                Log.Descripcion1 = JX.Message;
                Log.Descripcion2 = "error consultar el idcliente";
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);
            }



            Log.TipoError = TipoError.info;
            Log.Nombre = leads.telefono;
            Log.log_json = serializar.Serialize(leads);
            Log.Descripcion2 = PresenceCarga.CustomData3;
            Log.Descripcion3 = PresenceCarga.Cogido1producto;
            Log.Descripcion4 = PresenceCarga.sourceId.ToString();
            Resultado = Log.Enviar(Log);




            try
            {


                if (!ModelState.IsValid)
                {

                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select new { Message = item.Value.Errors[0].ErrorMessage, field = item.Key }).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if (leads.producto != null && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }




                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;



                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.

                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = leads.telefono;
                    Log.Descripcion1 = ext.Message;
                    Log.Descripcion2 = "error al insertar en presence";
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }



        /// <summary>
        /// Web service solicitado por claro.
        /// </summary>
        /// <param name="leads"></param>
        /// <returns></returns>
        [Route("ingresaSolicitud")]
        public IHttpActionResult  PostLeads(Leads leads)
        {

            /*------------------------------------------
             * ACTUALIZANDO PARAMETROS CARGA.
             -------------------------------------------*/
            var serializar = new JavaScriptSerializer();
            var Log = new log()
            {
                Nombre = "Nuevo lead de ingresar solicitud",
                log_json = serializar.Serialize(leads),
                seccionId = 1,
                TipoError = TipoError.info
            };
            ResponseLog Resultado = new ResponseLog() { };


            if (leads.referencia_producto.Contains("CTC") && leads.utm_campaign != "convergencia")
            {
                campaign = db.Campaign.Find(5);
                PresenceCarga.carga = campaign.Carga;
                PresenceCarga.servicio = campaign.Servicio;
            }
            if (leads.producto.Contains("Renueva tu equipo") && leads.utm_campaign != "convergencia")
            {
                campaign = db.Campaign.Find(6);
                PresenceCarga.carga = campaign.Carga;
                PresenceCarga.servicio = campaign.Servicio;
            }
            if (leads.producto.Contains("Equipos") && leads.utm_campaign != "convergencia")
            {
                campaign = db.Campaign.Find(6);
                PresenceCarga.carga = campaign.Carga;
                PresenceCarga.servicio = campaign.Servicio;
            }

            if (leads.utm_campaign == "convergencia")
            {
                campaign = db.Campaign.Find(7);
                PresenceCarga.carga = campaign.Carga;
                PresenceCarga.servicio = campaign.Servicio;
            }
            leads.telefono = VerificarTelefono.Verificar(leads.telefono, leads.ciudad);



            leads.Servicio = campaign.Servicio.ToString();
            leads.Carga = campaign.Carga.ToString();
            try
            {


                if (!ModelState.IsValid)
                {

                    var  errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select  new { Message = item.Value.Errors[0].ErrorMessage , field = item.Key } ).ToList();
                    var d = new { codigo = -1, Mensaje = errorList };
                    Log.TipoError = TipoError.warning;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = serializar.Serialize(errorList);
                    Resultado = Log.Enviar(Log);

                    return Ok(d);
                }

                // insertar registro en presence

                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.telefono = leads.telefono;
                PresenceCarga.CustomData2 = leads.producto;

                if(leads.producto != null  && leads.producto.Length > 100)
                {
                    PresenceCarga.Comentarios = "pro_ws=" +  leads.producto.Substring(0 , 100);

                }
                else
                {
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                }

                /*------------------------------------------
                 * VERIFICAR EN PRESENCE.
                 -------------------------------------------*/
                PresenceCarga.Cogido1producto = sppliter.split(leads.referencia_producto);


                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
                leads.PresenceMessage = RespuestaPresence;
                leads.SourceIdPresence = PresenceCarga.sourceId.ToString();
                leads.CampaignId = campaign.Id;

                Log.TipoError = TipoError.info;
                Log.Descripcion1 = leads.telefono;
                Log.Descripcion2 = leads.nombres;
                Log.Descripcion3 = PresenceCarga.Cogido1producto;
                Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                Resultado = Log.Enviar(Log);

            
                // tratando de guardar en la base de datos
                db.Leads.Add(leads);
                db.Database.CommandTimeout = 300;
                db.SaveChanges();


            }
            catch (Exception ex)
            {

                string fecha = DateTime.Now.ToString("yyyyMMdd");
                string fechaCompleta = DateTime.Now.ToString();

                string Evidencia = "logsSolicitud" + fecha + ".txt";

                var ruta = Path.Combine(HttpContext.Current.Server.MapPath("~/ArchivoLogs/"), Path.GetFileName(Evidencia));

                using (FileStream fs = new FileStream(ruta, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(fechaCompleta);
                    sw.WriteLine(ex.ToString());
                }

                try
                {
                    db.Database.CommandTimeout = 300;
                    db.Leads.Add(leads);
                    db.SaveChanges();
                    // INSERTAR EN BD CLAROCOL.
   
                }
                catch (Exception ext)
                {
                    Log.TipoError = TipoError.error;
                    Log.Nombre = ext.Message;
                    Log.Descripcion1 = leads.telefono;
                    Log.Descripcion2 = leads.nombres;
                    Log.Descripcion3 = PresenceCarga.Cogido1producto;
                    Log.Descripcion4 = PresenceCarga.sourceId.ToString();
                    Resultado = Log.Enviar(Log);

                    var d = new { codigo = 0, Mensaje = "Success" };
                    return Ok(d);
                }
            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v) ;
        }

        public class queryString
        {

            public string Data { get; set; }
        }

        // DELETE: api/Leads/5
        [ResponseType(typeof(Leads))]
        public IHttpActionResult DeleteLeads(int id)
        {
            Leads leads = db.Leads.Find(id);
            if (leads == null)
            {
                return NotFound();
            }

            db.Leads.Remove(leads);
            db.SaveChanges();

            return Ok(leads);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeadsExists(int id)
        {
            return db.Leads.Count(e => e.Id == id) > 0;
        }




    }

 


}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using Core.Internal.Logs;
using leads_claro_colombia.Models;
using leads_claro_colombia.Models.leads;
using leads_claro_colombia.ViewModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Http.Cors;

namespace leads_claro_colombia.Controllers
{
    public class InsertarPresenceController : ApiController
    {

        private static ServicePresence.PcowsServiceClient WS;
        public PresenceParams PresenceCarga = new PresenceParams() { };

        public InsertarPresenceController()
        {

        }
        


        private ApplicationDbContext db = new ApplicationDbContext();



        // GET: api/Leads/5
        [Route("LeadNoPresence")]
        [ResponseType(typeof(Leads))]
        public IHttpActionResult GetLeads()
        {
            string fechaInicial = DateTime.Now.AddDays(-2).ToString("yyyyMMdd");
            string fechaFinal = DateTime.Now.AddDays(1).ToString("yyyyMMdd");

            //INSERTAR EN BD CLAROCOL.
            List<queryString> data = db.Database.SqlQuery<queryString>("P_LeadsNoPresence @fechaInicio = {0},@fechaFin = {1}", fechaInicial, fechaFinal).ToList();


            foreach (var item in data)
            {
                // INSERTAR LEADS FALTANTES EN PRESENCE
                PostIngresarPresence(item);
            }



            return Ok(data);
        }




        /*------------------------------------------
         * API PARA INGRESAR EL SERVICIO DE TECNOLOGIA 
         -------------------------------------------*/

        // POST: api/Leads
        //[ResponseType(typeof(Leads))]
        [Route("IngresarPresence")]
        public IHttpActionResult PostIngresarPresence(queryString leads)
        {


            //var serializar = new JavaScriptSerializer();
            //var Log = new log()
            //{
            //    Nombre = "Nuevo lead de ingresar Tecnologia",
            //    log_json = serializar.Serialize(leads),
            //    seccionId = 2,
            //    TipoError = TipoError.info
            //};
            //ResponseLog Resultado = new ResponseLog() { };




            try
            {

                PresenceCarga.servicio = (int)leads.servicio;
                PresenceCarga.carga = (int)leads.Carga;
                PresenceCarga.telefono =leads.telefono;
                /*------------------------------------------
                * VERIFICAR EN PRESENCE.
                -------------------------------------------*/


                char asterisk = '*';
                int Refecenciaproductocodigo = 0;


                if(leads.referencia_producto != null)
                {
                    var codigosReferenciaProducto = leads.referencia_producto.Split(asterisk);
                    if (codigosReferenciaProducto != null && codigosReferenciaProducto[0] != null)
                    {
                        //Int32.TryParse(codigosReferenciaProducto[0].ToString(), out Refecenciaproductocodigo);
                        PresenceCarga.Cogido1producto = codigosReferenciaProducto[0];
                    }
                }
                else
                {
                    PresenceCarga.Cogido1producto = leads.id.ToString();
                }



                PresenceCarga.nombreCliente = leads.nombres + " " + leads.apellidos;
                PresenceCarga.CustomData2 = leads.producto;
                PresenceCarga.sourceId = Int32.Parse(leads.SourceIdPresence);
                if (leads.producto != null && leads.producto.Length > 100)
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto.Substring(0, 100);
                else
                    PresenceCarga.Comentarios = "pro_ws=" + leads.producto;

                string RespuestaPresence = PresenceCarga.InsertarPresence(PresenceCarga);
            }
            catch (Exception ex)
            {


            }

            var v = new { codigo = 0, Mensaje = "Success" };
            return Ok(v);
        }


        public class queryString
        {

            public int? id { get; set; }
            public string nombres { get; set; }
            public string producto { get; set; }
            public string apellidos { get; set; }
            public string SourceIdPresence { get; set; }
            public string telefono { get; set; }
            public int? idCampana { get; set; }
            public string campana { get; set; }
            public int? servicio { get; set; }
            public int? Carga { get; set; }
            public string Fecha { get; set; }
            public string Hora { get; set; }
            public string referencia_producto { get; set; }
        }

        // DELETE: api/Leads/5
        [ResponseType(typeof(Leads))]
        public IHttpActionResult DeleteLeads(int id)
        {
            Leads leads = db.Leads.Find(id);
            if (leads == null)
            {
                return NotFound();
            }

            db.Leads.Remove(leads);
            db.SaveChanges();

            return Ok(leads);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeadsExists(int id)
        {
            return db.Leads.Count(e => e.Id == id) > 0;
        }



        public class PresenceParams
        {
            public int servicio { get; set; }
            public int carga { get; set; }
            public int sourceId { get; set; } = Math.Abs(unchecked((int)DateTime.Now.Ticks));
            public string nombreCliente { get; set; }
            public string telefono { get; set; }
            public string Comentarios { get; set; }
            public string Cogido1producto { get; set; }

            public string CustomData2 { get; set; }
            
            public string CustomData3 { get; set; }
             /**
                 * METODO PARA INSERTAR LA LLAMDA A PRESENCE
                 */
            public string InsertarPresence(PresenceParams P)
            {
                int resp_ws;
                string Respuesta;
                try
                {

                    /*
                    * CODIGO ORIGINAL.
                    */

                    //WS = new ServicePresence.PcowsServiceClient();
                    //resp_ws = WS.InsertOutboundRecord(P.servicio, P.carga, P.sourceId, P.nombreCliente, P.telefono.ToString(), 1, DateTime.Now, 0, 100, P.Comentarios);

                    //Respuesta = "1";

                    /*
                     * PRUEBA DE WS SERVICES con custom data.
                     */



                    WS = new ServicePresence.PcowsServiceClient();

                    resp_ws = WS.InsertOutboundRecord4(

                        P.servicio,  //int ServiceId,
                         P.carga,         //int LoadId,
                          P.sourceId,         //int SourceId,
                          P.nombreCliente,        //string Name,
                          null,        //string TimeZone,
                            1,      //int Status,
                           P.telefono.ToString(),       //string Phone,
                           null,       //string PhoneTimeZone,
                           null,       //string AlternativePhones,
                           null,       //string AlternativePhoneDescriptions,
                           null,       //string AlternativePhoneTimeZones,
                           DateTime.Now,            //System.DateTime ScheduleDate,
                            0,                      //long CapturingAgent,
                            100,                    //int Priority,
                            P.Comentarios,          //string Comments,
                            P.Cogido1producto,      //string CustomData1,
                            P.CustomData2,          //string CustomData2,
                            P.CustomData3,          //string CustomData3,
                            null,                   //string CallerId,
                            null,                   //string CallerName,
                             false                  //bool AutomaticTimeZoneDetection
                        );

                    Respuesta = "1";



                }
                catch (Exception ex)
                {
                    Respuesta = "Error al Registrar Llamada en Presence." + ex.Message;
                }
                return Respuesta;
            }


        }
    }

 


}
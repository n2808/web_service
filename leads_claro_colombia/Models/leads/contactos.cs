﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace leads_claro_colombia.Models.leads
{
    public class Contactos : EntityWithIntId
    {
        [Required]
        public string nombres { get; set; }
        [Required]
        public string telefono { get; set; }
        public string cliente { get; set; }
        public string Plan { get; set; }
        public string Operador { get; set; }
        public string Servicio { get; set; }
        public string Carga { get; set; }

        public string PresenceMessage { get; set; }
        public string SourceIdPresence { get; set; }
        [Required]
        public int CampaignId { get; set; }
        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }
    }
}
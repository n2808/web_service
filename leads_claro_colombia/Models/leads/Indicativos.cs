﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace leads_claro_colombia.Models.leads
{
    [Table("Indicativos")]
    public class Indicativos : EntityWithIntId
    {
        public int Indicativo { get; set; }
        public string ciudad { get; set;  }
        public string departamento { get; set; }
    }
}
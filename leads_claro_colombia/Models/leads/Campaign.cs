﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace leads_claro_colombia.Models.leads
{
    public class Campaign : EntityWithIntId
    {
        public string Nombre { get; set; }
        public Boolean Estado { get; set; }
        public int Tiempo_ACW { get; set; }
        public string Second_Tab { get; set; }
        public string Tab_Names { get; set; }
        public int Servicio { get; set; }
        public int Carga { get; set; }
        public string Prefijo { get; set; }
        public int? NumCamposOpcionales { get; set; }
        public int? PrefijoCelular { get; set; }
        public int? DigitosCelular { get; set; }
        public int? PrefijoFijo { get; set; }
        public int? DigitosFijo { get; set; }
        public int? SourceId { get; set; }
        public string token { get; set; }

    
    }
}
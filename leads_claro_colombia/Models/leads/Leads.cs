﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace leads_claro_colombia.Models.leads
{
    public class Leads : EntityWithIntId
    {
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string cedula { get; set; }
    
        public string ciudad { get; set; }
        public string email { get; set; }
       
        public int tipo_telefono { get; set; }
        [Required]
        public string telefono { get; set; }
        public string extension { get; set; }
        public string producto { get; set; }

        [Required(ErrorMessage = "El campo optin es obligatorio"), Range(0, 99999, ErrorMessage = "Por favor ingrese un número valido")]
        public int optin { get; set; }

        public string glid { get; set; }
        public string utm_source { get; set; }
        public string utm_campaign { get; set; }
        public string utm_medium { get; set; }
        public string queu_promo { get; set; }
        public string referencia_producto { get; set; }
        public string canal_trafico { get; set; }
        public string resumen_plan { get; set; }
        public string PresenceMessage { get; set; }
        public string SourceIdPresence { get; set; }
        public string Servicio { get; set; }
        public string Carga { get; set; }

        public int? CampaignId { get; set; }

        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }
    }
}
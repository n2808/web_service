﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace leads_claro_colombia.ViewModel
{
    public class WebHubGoogleVM
    {
        public string lead_id { get; set; }
        public string form_id { get; set; }
        public List<UserLeadColumnData> user_column_data { get; set; }
        public string api_version { get; set; }
        public string google_key { get; set; }
    }

    public class UserLeadColumnData
    {
        public string column_name { get; set; }
        public string string_value { get; set; }
    }
}